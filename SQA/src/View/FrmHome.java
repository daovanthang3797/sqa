/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.SubjectDAO;
import Model.Result;
import Model.Student;
import Model.Subject;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author daova
 */
public class FrmHome extends javax.swing.JFrame {

    /**
     * Creates new form FrmHome
     */
    public static Student sinhvien;

    public Student getSinhvien() {
        return sinhvien;
    }

    public void setSinhvien(Student sinhvien) {
        FrmHome.sinhvien = sinhvien;
    }

    public FrmHome() {
        initComponents();

    }

    public ArrayList<Subject> removeFSubject(ArrayList<Subject> list) {
        ArrayList<String> listsubject = new ArrayList();
        listsubject.add("BAS1107");
        listsubject.add("BAS1106");
        listsubject.add("SKD1103");
        listsubject.add("SKD1102");
        listsubject.add("SKD1101");
        for (int i = 0; i < list.size(); i++) {
            if (listsubject.contains(list.get(i).getCode())) {
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public boolean checkCode(String code) {
        Pattern pattern = Pattern.compile("\\d*");
        Matcher matcher = pattern.matcher(code);
        if (matcher.matches() || code.equalsIgnoreCase("all")) {
            if (code.length() == 5 || code.equalsIgnoreCase("all")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static float getDiembyDiemChu(String s) {
        float diem = 0;
        if (s.equalsIgnoreCase("F")) {
            diem = 0;
        }
        if (s.equalsIgnoreCase("D")) {
            diem = 1;
        }
        if (s.equalsIgnoreCase("D+")) {
            diem = (float) 1.5;
        }
        if (s.equalsIgnoreCase("C")) {
            diem = 2;
        }
        if (s.equalsIgnoreCase("C+")) {
            diem = (float) 2.5;
        }
        if (s.equalsIgnoreCase("B")) {
            diem = 3;
        }
        if (s.equalsIgnoreCase("B+")) {
            diem = (float) 3.5;
        }
        if (s.equalsIgnoreCase("A")) {
            diem = (float) 3.7;
        }
        if (s.equalsIgnoreCase("A+")) {
            diem = (float) 4;
        }
        return diem;
    }

    public ArrayList<Subject> getDiem(String makyhoc) {
        ArrayList<Subject> arrayList = new ArrayList<>();
        if (this.checkCode(makyhoc) == true) {
            SubjectDAO sub = new SubjectDAO();
            arrayList = sub.getAllbyMsv(this.getSinhvien().getCode());
            for (int j = 0; j < arrayList.size(); j++) {
                if (arrayList.get(j).getPer_CC() != null) {
                    if (arrayList.get(j).getPer_CC().contains(".0")) {
                        String replace = arrayList.get(j).getPer_CC().replace(".0", "");
                        arrayList.get(j).setPer_CC(replace);
                    } else if (arrayList.get(j).getPer_CC().equalsIgnoreCase("")) {
                        arrayList.get(j).setPer_CC("0");
                    }
                } else {
                    arrayList.get(j).setPer_CC("0");
                }

                if (arrayList.get(j).getPer_BT() != null) {
                    if (arrayList.get(j).getPer_BT().contains(".0")) {
                        String replace = arrayList.get(j).getPer_BT().replace(".0", "");
                        arrayList.get(j).setPer_BT(replace);
                    } else if (arrayList.get(j).getPer_BT().equalsIgnoreCase("")) {
                        arrayList.get(j).setPer_BT("0");
                    }
                } else {
                    arrayList.get(j).setPer_BT("0");
                }

                if (arrayList.get(j).getPer_KT() != null) {
                    if (arrayList.get(j).getPer_KT().contains(".0")) {
                        String replace = arrayList.get(j).getPer_KT().replace(".0", "");
                        arrayList.get(j).setPer_KT(replace);
                    } else if (arrayList.get(j).getPer_KT().equalsIgnoreCase("")) {
                        arrayList.get(j).setPer_KT("0");
                    }
                } else {
                    arrayList.get(j).setPer_KT("0");
                }

                if (arrayList.get(j).getPer_TH() != null) {
                    if (arrayList.get(j).getPer_TH().contains(".0")) {
                        String replace = arrayList.get(j).getPer_TH().replace(".0", "");
                        arrayList.get(j).setPer_TH(replace);
                    } else if (arrayList.get(j).getPer_TH().equalsIgnoreCase("")) {
                        arrayList.get(j).setPer_TH("0");
                    }
                } else {
                    arrayList.get(j).setPer_TH("0");
                }

                if (arrayList.get(j).getPer_Thi() != null) {
                    if (arrayList.get(j).getPer_Thi().contains(".0")) {
                        String replace = arrayList.get(j).getPer_Thi().replace(".0", "");
                        arrayList.get(j).setPer_Thi(replace);
                    } else if (arrayList.get(j).getPer_Thi().equalsIgnoreCase("")) {
                        arrayList.get(j).setPer_Thi("0");
                    }
                } else {
                    arrayList.get(j).setPer_Thi("0");
                }

            }
            if (makyhoc.equalsIgnoreCase("all")) {
                return arrayList;
            } else {
                for (int i = 0; i < arrayList.size(); i++) {
                    if (!arrayList.get(i).getKy_hoc().equalsIgnoreCase(makyhoc)) {
                        arrayList.remove(i);
                        i--;
                    }
                }
                return arrayList;
            }
        } else {
            return arrayList;
        }
    }

//    public ArrayList<Subject> convertListSuject(ArrayList<Subject> listIn) {
//        ArrayList<Subject> listOut = new ArrayList();
//        for (int i = 0; i < listIn.size(); i++) {
//            if (!listOut.contains(listIn.get(i))) {
//                listOut.add(listIn.get(i));
//            } else {
//                int x = 0;
//                for (int j = 0; j < listOut.size(); j++) {
//                    if (listIn.get(i).getCode().equalsIgnoreCase(listOut.get(j).getCode())) {
//                        x = j;
//                        break;
//                    }
//                }
//                float num_listIn = Float.parseFloat(listIn.get(i).getTK_num());
//                float num_listOut = Float.parseFloat(listOut.get(x).getTK_num());
//                if (num_listIn > num_listOut) {
//                    listOut.remove(x);
//                    x--;
//                    listOut.add(listIn.get(i));
//                    i=0;
//                }
//            }
//        }
//        return listOut;
//    }

    public ArrayList getTongKet(ArrayList<Subject> arrayList, String kyhoc) {
//        ArrayList<Subject> arrayList = convertListSuject(arrayList1);
        ArrayList<String> listKH = new ArrayList<>();
        for (int v = 0; v < arrayList.size(); v++) {
            if (!listKH.contains(arrayList.get(v).getKy_hoc())) {
                listKH.add(arrayList.get(v).getKy_hoc());
            }
        }
        ArrayList<Result> listRs = new ArrayList<>();
        for (int x = 0; x < listKH.size(); x++) {
            Result rs = new Result();
            int total_tc_now = 0;
            float he10now = 0;
            float he4now = 0;
            float he10 = 0;
            float he4 = 0;
            int tc_dat = 0;
            int tich_luy;
            int tongtche4now = 0;
            int tongtcnow = 0;
            int tongtc = 0;
            float total10 = 0;
            rs.setKyhoc(listKH.get(x));
            for (Subject sj : arrayList) {
                if (sj.getKy_hoc().equalsIgnoreCase(listKH.get(x))) {
//                    if (!sj.getTK_String().equalsIgnoreCase("F")) {
                        he4now = he4now + sj.getSoTC() * getDiembyDiemChu(sj.getTK_String());
                        tongtche4now += sj.getSoTC();
//                    }

//                    if (!sj.getTK_String().equalsIgnoreCase("F")) {
                        he10now = he10now + sj.getSoTC() * Float.parseFloat(sj.getTK_num());
                        tongtcnow += sj.getSoTC();

//                    }
                    if (!sj.getTK_String().equalsIgnoreCase("F")) {
                        total_tc_now = total_tc_now + sj.getSoTC();
                    }
                    if (!sj.getTK_String().equalsIgnoreCase("F")) {
                        tc_dat = tc_dat + sj.getSoTC();
                    }
                    if (!sj.getTK_String().equalsIgnoreCase("F")) {
                        total10 = total10 + sj.getSoTC() * Float.parseFloat(sj.getTK_num());
                        he4 = he4 + sj.getSoTC() * getDiembyDiemChu(sj.getTK_String());
                    }
                }
                if (!sj.getTK_String().equalsIgnoreCase("F")) {
                    he10 = he10 + sj.getSoTC() * Float.parseFloat(sj.getTK_num());
                    tongtc = tongtc + sj.getSoTC();
                }

            }
            float tmphe10now = (float) Math.round(((float) he10now / tongtcnow) * 100) / 100;
            float tmphe4now = (float) Math.round(((float) he4now / tongtche4now) * 100) / 100;
            rs.setTotal10(total10);
            rs.setTotal4(he4);
            rs.setSotc(total_tc_now);
            for (int y = 0; y < listRs.size(); y++) {
                tc_dat = tc_dat + listRs.get(y).getSotc();
            }
            rs.setHe4(tmphe4now);
            rs.setTongtctichluy(tc_dat);
            rs.setHe10(tmphe10now);
//                rs.setTichluy10(tmphe10);
            float totalhe10 = he10now;
            float totalhe4 = he4now;
            for (int z = 0; z < listRs.size(); z++) {
                totalhe10 = totalhe10 + listRs.get(z).getTotal10();
                totalhe4 = totalhe4 + listRs.get(z).getTotal4();
            }
            float tmphe10 = (float) Math.round(((float) totalhe10 / rs.getTongtctichluy()) * 100) / 100;
            float tmphe4 = (float) Math.round(((float) totalhe4 / rs.getTongtctichluy()) * 100) / 100;
            rs.setTichluy10(tmphe10);
            rs.setTichluy4(tmphe4);

            listRs.add(rs);
            for (Result listR : listRs) {
                if (!listR.getKyhoc().equalsIgnoreCase(kyhoc)) {
                    arrayList.remove(listR);
                }
            }
        }
        return listRs;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        hello = new javax.swing.JLabel();
        fill = new javax.swing.JLabel();
        view = new javax.swing.JButton();
        code = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        hello.setText("Xin chào");

        fill.setText("Nhập học kỳ muốn xem điểm");

        view.setText("Xem điểm");
        view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewActionPerformed(evt);
            }
        });
        view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                viewKeyPressed(evt);
            }
        });

        code.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codeActionPerformed(evt);
            }
        });

        jButton1.setText("<< Quay lại");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Nhập mã kỳ học theo định dạng: [năm học][kỳ học] ví dụ :20172");

        jLabel2.setText("Trường hợp xem tất cả các kỳ mời nhập code = all");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(168, 168, 168)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(183, 183, 183))
            .addGroup(layout.createSequentialGroup()
                .addGap(212, 212, 212)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(code)
                    .addComponent(fill, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(221, 221, 221))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(197, 197, 197)
                .addComponent(hello, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(198, 198, 198))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(251, 251, 251)
                        .addComponent(view, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(262, 262, 262))
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(38, 38, 38)
                .addComponent(hello, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(58, 58, 58)
                .addComponent(fill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(code)
                .addGap(18, 18, 18)
                .addComponent(view, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(42, 42, 42)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(59, 59, 59))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void codeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_codeActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        FrmLogin frLogin = new FrmLogin();
        frLogin.setLocationRelativeTo(null);
        frLogin.setVisible(rootPaneCheckingEnabled);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewActionPerformed
        // TODO add your handling code here:
//        calcul();
        if (checkCode(this.getCode().getText())) {
            FrmDiem frmDiem = new FrmDiem();
            frmDiem.setLocationRelativeTo(null);
            ArrayList<Subject> arrayList = this.getDiem(this.getCode().getText());
            DefaultTableModel model = (DefaultTableModel) frmDiem.getjTable1().getModel();
            model.setRowCount(0);
            for (Subject s : arrayList) {
                model.addRow(s.toObject());
            }
            ArrayList<Result> listRs = this.getTongKet(removeFSubject(this.getDiem("all")), this.getCode().getText());
            DefaultTableModel model2 = (DefaultTableModel) frmDiem.getjTable2().getModel();
            model2.setRowCount(0);
            for (Result s : listRs) {
                if (this.getCode().getText().equalsIgnoreCase("all")) {
                    model2.addRow(s.toObject());
                } else {
                    if (s.getKyhoc().equalsIgnoreCase(this.getCode().getText())) {
                        model2.addRow(s.toObject());
                    }
                }
            }
            frmDiem.setListSj(arrayList);
            frmDiem.setListRs(listRs);
            frmDiem.setName(this.getSinhvien().getName());
            frmDiem.setVisible(rootPaneCheckingEnabled);
        } else {
            JOptionPane.showMessageDialog(null, "Mã kỳ học không đúng định dạng");
        }

    }//GEN-LAST:event_viewActionPerformed

    private void viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_viewKeyPressed
        if (checkCode(this.getCode().getText())) {
            FrmDiem frmDiem = new FrmDiem();
            frmDiem.setLocationRelativeTo(null);
            ArrayList<Subject> arrayList = this.getDiem(this.getCode().getText());
            DefaultTableModel model = (DefaultTableModel) frmDiem.getjTable1().getModel();
            model.setRowCount(0);
            for (Subject s : arrayList) {
                model.addRow(s.toObject());
            }
            ArrayList<Result> listRs = this.getTongKet(removeFSubject(this.getDiem("all")), this.getCode().getText());
            DefaultTableModel model2 = (DefaultTableModel) frmDiem.getjTable2().getModel();
            model2.setRowCount(0);
            for (Result s : listRs) {
                if (this.getCode().getText().equalsIgnoreCase("all")) {
                    model2.addRow(s.toObject());
                } else {
                    if (s.getKyhoc().equalsIgnoreCase(this.getCode().getText())) {
                        model2.addRow(s.toObject());
                    }
                }
            }
            frmDiem.setListSj(arrayList);
            frmDiem.setListRs(listRs);
            frmDiem.setName(this.getSinhvien().getName());
            frmDiem.setVisible(rootPaneCheckingEnabled);
        } else {
            JOptionPane.showMessageDialog(null, "Mã kỳ học không đúng định dạng");
        }
    }//GEN-LAST:event_viewKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmHome frmHome = new FrmHome();
                frmHome.setLocationRelativeTo(null);
                frmHome.setVisible(true);

            }
        });
    }

    public JTextField getCode() {
        return code;
    }

    public void setCode(JTextField code) {
        this.code = code;
    }

    public JLabel getFill() {
        return fill;
    }

    public void setFill(JLabel fill) {
        this.fill = fill;
    }

    public JLabel getHello() {
        return hello;
    }

    public void setHello(JLabel hello) {
        this.hello = hello;
    }

    public JButton getjButton1() {
        return jButton1;
    }

    public void setjButton1(JButton jButton1) {
        this.jButton1 = jButton1;
    }

    public JLabel getjLabel1() {
        return jLabel1;
    }

    public void setjLabel1(JLabel jLabel1) {
        this.jLabel1 = jLabel1;
    }

    public JLabel getjLabel2() {
        return jLabel2;
    }

    public void setjLabel2(JLabel jLabel2) {
        this.jLabel2 = jLabel2;
    }

    public JButton getView() {
        return view;
    }

    public void setView(JButton view) {
        this.view = view;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField code;
    private javax.swing.JLabel fill;
    private javax.swing.JLabel hello;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton view;
    // End of variables declaration//GEN-END:variables
}
