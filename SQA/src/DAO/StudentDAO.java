/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daova
 */
public class StudentDAO implements DAO<Student> {
//    public static ConnectDB con = null;
    public StudentDAO() {
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            String sql = "set global max_connections = " + 6000;
            Statement stmt = connect.createStatement();
            stmt.executeQuery(sql);

        } catch (SQLException ex) {
        } catch (Exception ex) {
        }
    }
    public Student getStudentbyCode(String code){
        Student s= new Student();
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM student where code='"+code+"'");
            rs.next();
            s.setCode(code);
            s.setName(rs.getString("name"));
            s.setPass(rs.getString("pass"));
            System.out.println(s);
        } catch (Exception ex) {
        }
        return s;
    }
    public boolean checkLogin(String msv,String pass){
        boolean kiem_tra = false;
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) AS total FROM student where code='"+msv+"' and pass='"+pass+"'");
            rs.next();
            int total = rs.getInt("total");
            System.out.println(total);
            if(total!=0){
                kiem_tra=true;
            }
        } catch (Exception ex) {
        }
        return kiem_tra;
    }
    public static int getIdSinhVienbyMsv(String msv){
        int idSinhvien =0;       
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT idSinhVien FROM student where code = '"+msv+"'");
            while (rs.next()) {
                return rs.getInt("idSinhVien");
            }
        } catch (Exception ex) {
        }
        return idSinhvien;
    }

    public static boolean checkMsv(String msv) {
        boolean kiem_tra = false;
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT code FROM student");
            while (rs.next()) {
                String code = rs.getString("code");
                if (code.equalsIgnoreCase(msv)) {
//                    con.closeConnect();
                    kiem_tra = true;
                }
            }
        } catch (Exception ex) {
        }

        return kiem_tra;
    }

    @Override
    public void insert(Student t) {
        if (checkMsv(t.getCode()) == false) {
            try {
                ConnectDB con = new ConnectDB();
                Connection connect = con.openConnect();
                PreparedStatement preparedStatement = connect.prepareStatement("insert into student values(?, ?, ?)");
                preparedStatement.setString(1, t.getCode());
                preparedStatement.setString(2, t.getName());
                preparedStatement.setString(3, t.getPass());
                preparedStatement.executeUpdate();
//                con.closeConnect();
            } catch (Exception ex) {

            }
        }

    }

    @Override
    public void update(Student t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Student t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getAll() {
        ConnectDB con = null;
        try {
            con = new ConnectDB();
        } catch (Exception ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM student");
            while (rs.next()) {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3) + rs.getInt(4));
            }
//            con.closeConnect();
        } catch (Exception ex) {

        }
    }

//    public static void main(String[] args) {
//        StudentDAO studentDAO = new StudentDAO();
//        System.out.println(studentDAO.getStudentbyCode("b15dccn494"));  
//    }
}
