/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author daova
 */
public interface DAO<T> {
    void getAll();
    
    void insert(T t);
 
    void update(T t);
 
    void delete(T t);
}
