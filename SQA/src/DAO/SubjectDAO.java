/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Student;
import Model.Subject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daova
 */
public class SubjectDAO implements DAO<Subject> {

//    public static ConnectDB con = new ConnectDB();

    public static boolean checkMsvAndCode(String msv, String code) {
        boolean kiem_tra = false;
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) AS total FROM subject where msv='"+msv+"' and code='"+code+"'");
            rs.next();
            int total = rs.getInt("total");
            System.out.println(total);
            if(total!=0){
                kiem_tra=true;
            }
        } catch (Exception ex) {
        }

        return kiem_tra;
    }

    @Override
    public void insert(Subject t) {
        if (checkMsvAndCode(t.getMsv(), t.getCode()) == false) {
            try {
                ConnectDB con = new ConnectDB();
                Connection connect = con.openConnect();
                PreparedStatement preparedStatement = connect.prepareStatement("insert into subject values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)");
                preparedStatement.setString(1, t.getCode());
                preparedStatement.setString(2, t.getName());
                preparedStatement.setInt(3, t.getSoTC());
                preparedStatement.setString(4, t.getPer_CC());
                preparedStatement.setString(5, t.getPer_KT());
                preparedStatement.setString(6, t.getPer_TH());
                preparedStatement.setString(7, t.getPer_BT());
                preparedStatement.setString(8, t.getPer_Thi());
                preparedStatement.setString(9, t.getCC());
                preparedStatement.setString(10, t.getKT());
                preparedStatement.setString(11, t.getTH());
                preparedStatement.setString(12, t.getBT());
                preparedStatement.setString(13, t.getThi_1());
                preparedStatement.setString(14, t.getThi_2());
                preparedStatement.setString(15, t.getTK_num());
                preparedStatement.setString(16, t.getTK_String());
                preparedStatement.setString(17, t.getKy_hoc());
                preparedStatement.setString(18, t.getMsv());
                preparedStatement.executeUpdate();
                con.closeConnect();
            } catch (Exception ex) {

            }
        }

    }

    @Override
    public void update(Subject t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Subject t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public static ArrayList<Subject> getAllbyKyhoc(String msv,String kyhoc){
        ArrayList<Subject> subjects = new ArrayList<Subject>();
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM subject where msv='"+msv+"' and ky_hoc='"+kyhoc+"' ORDER BY ky_hoc");
            while(rs.next()){
                Subject sub = new Subject();
                sub.setCode(rs.getString("code"));
                sub.setName(rs.getString("name"));
                sub.setSoTC(rs.getInt("soTC"));
                sub.setPer_CC(rs.getString("per_CC"));
                sub.setPer_KT(rs.getString("per_KT"));
                sub.setPer_TH(rs.getString("per_TH"));
                sub.setPer_BT(rs.getString("per_BT"));
                sub.setPer_Thi(rs.getString("per_Thi"));
                sub.setCC(rs.getString("CC"));
                sub.setKT(rs.getString("KT"));
                sub.setTH(rs.getString("TH"));
                sub.setBT(rs.getString("BT"));
                sub.setThi_1(rs.getString("Thi_1"));
                sub.setThi_2(rs.getString("Thi_2"));
                sub.setTK_num(rs.getString("TK_num"));
                sub.setTK_String(rs.getString("TK_string"));
                sub.setKy_hoc(kyhoc);
                sub.setMsv(msv);
                subjects.add(sub);
                
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return subjects;
    }
    public ArrayList<Subject> getAllbyMsv(String msv){
        ArrayList<Subject> subjects = new ArrayList<Subject>();
        try {
            ConnectDB con = new ConnectDB();
            Connection connect = con.openConnect();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM subject where msv='"+msv+"' ORDER BY ky_hoc");
            while(rs.next()){
                Subject sub = new Subject();
                sub.setCode(rs.getString("code"));
                sub.setName(rs.getString("name"));
                sub.setSoTC(rs.getInt("soTC"));
                sub.setPer_CC(rs.getString("per_CC"));
                sub.setPer_KT(rs.getString("per_KT"));
                sub.setPer_TH(rs.getString("per_TH"));
                sub.setPer_BT(rs.getString("per_BT"));
                sub.setPer_Thi(rs.getString("per_Thi"));
                sub.setCC(rs.getString("CC"));
                sub.setKT(rs.getString("KT"));
                sub.setTH(rs.getString("TH"));
                sub.setBT(rs.getString("BT"));
                sub.setThi_1(rs.getString("Thi_1"));
                sub.setThi_2(rs.getString("Thi_2"));
                sub.setTK_num(rs.getString("TK_num"));
                sub.setTK_String(rs.getString("TK_string"));
                sub.setKy_hoc(rs.getString("ky_hoc"));
                sub.setMsv(msv);
                subjects.add(sub);
                
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return subjects;
    }
//    public static void main(String[] args) {
//        SubjectDAO s = new SubjectDAO();
//        
////        System.out.println(s.checkMsvAndCode("B15DCCN494", "INT1303"));
//        System.out.println(SubjectDAO.getAllbyKyhoc("b15dccn494","20172"));
//        
//    }

}
