/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author daova
 */
public class Result {
    private String kyhoc;
    private float he10;
    private float he4;
    private float tichluy10;
    private float tichluy4;
    private int sotc;
    private int tongtctichluy;
    private float total10;
    private float total4;

    public float getTotal4() {
        return total4;
    }

    public void setTotal4(float total4) {
        this.total4 = total4;
    }

    public float getTotal10() {
        return total10;
    }

    public void setTotal10(float total10) {
        this.total10 = total10;
    }

    public float getHe10() {
        return he10;
    }

    public void setHe10(float he10) {
        this.he10 = he10;
    }

    public float getHe4() {
        return he4;
    }

    public void setHe4(float he4) {
        this.he4 = he4;
    }

    public float getTichluy10() {
        return tichluy10;
    }

    public void setTichluy10(float tichluy10) {
        this.tichluy10 = tichluy10;
    }

    public float getTichluy4() {
        return tichluy4;
    }

    public void setTichluy4(float tichluy4) {
        this.tichluy4 = tichluy4;
    }

    public int getSotc() {
        return sotc;
    }

    public void setSotc(int sotc) {
        this.sotc = sotc;
    }

    public int getTongtctichluy() {
        return tongtctichluy;
    }

    public void setTongtctichluy(int tongtctichluy) {
        this.tongtctichluy = tongtctichluy;
    }

    public Result() {
    }

    public Result(String kyhoc, float he10, float he4, float tichluy10, float tichluy4, int sotc, int tongtctichluy, float total10, float total4) {
        this.kyhoc = kyhoc;
        this.he10 = he10;
        this.he4 = he4;
        this.tichluy10 = tichluy10;
        this.tichluy4 = tichluy4;
        this.sotc = sotc;
        this.tongtctichluy = tongtctichluy;
        this.total10 = total10;
        this.total4 = total4;
    }





    public String getKyhoc() {
        return kyhoc;
    }

    public void setKyhoc(String kyhoc) {
        this.kyhoc = kyhoc;
    }
    public Object[] toObject() {
        return new Object[]{
           this.getKyhoc(),this.getHe10(),this.getHe4(),this.getTichluy10(),this.getTichluy4(),this.getSotc(),
            this.getTongtctichluy()
        };
    }    
}
