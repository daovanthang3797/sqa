/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author daova
 */
public class Subject {
    private String code;
    private String name;
    private int soTC;
    private String per_CC;
    private String per_KT;
    private String per_TH;
    private String per_BT;
    private String per_Thi;
    private String CC;
    private String KT;
    private String TH;
    private String BT;
    private String Thi_1;
    private String Thi_2;    
    private String TK_num;    
    private String TK_String;   
    private String ky_hoc;
    private String msv;

    public Subject() {
    }

    @Override
    public String toString() {
        return "Subject{" + "code=" + code + ", name=" + name + ", soTC=" + soTC + ", per_CC=" + per_CC + ", per_KT=" + per_KT + ", per_TH=" + per_TH + ", per_BT=" + per_BT + ", per_Thi=" + per_Thi + ", CC=" + CC + ", KT=" + KT + ", TH=" + TH + ", BT=" + BT + ", Thi_1=" + Thi_1 + ", Thi_2=" + Thi_2 + ", TK_num=" + TK_num + ", TK_String=" + TK_String + ", ky_hoc=" + ky_hoc + ", msv=" + msv + '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSoTC() {
        return soTC;
    }

    public void setSoTC(int soTC) {
        this.soTC = soTC;
    }

    public String getPer_CC() {
        return per_CC;
    }

    public void setPer_CC(String per_CC) {
        this.per_CC = per_CC;
    }

    public String getPer_KT() {
        return per_KT;
    }

    public void setPer_KT(String per_KT) {
        this.per_KT = per_KT;
    }

    public String getPer_TH() {
        return per_TH;
    }

    public void setPer_TH(String per_TH) {
        this.per_TH = per_TH;
    }

    public String getPer_BT() {
        return per_BT;
    }

    public void setPer_BT(String per_BT) {
        this.per_BT = per_BT;
    }

    public String getPer_Thi() {
        return per_Thi;
    }

    public void setPer_Thi(String per_Thi) {
        this.per_Thi = per_Thi;
    }

    public String getCC() {
        return CC;
    }

    public void setCC(String CC) {
        this.CC = CC;
    }

    public String getKT() {
        return KT;
    }

    public void setKT(String KT) {
        this.KT = KT;
    }

    public String getTH() {
        return TH;
    }

    public void setTH(String TH) {
        this.TH = TH;
    }

    public String getBT() {
        return BT;
    }

    public void setBT(String BT) {
        this.BT = BT;
    }

    public String getThi_1() {
        return Thi_1;
    }

    public void setThi_1(String Thi_1) {
        this.Thi_1 = Thi_1;
    }

    public String getThi_2() {
        return Thi_2;
    }

    public void setThi_2(String Thi_2) {
        this.Thi_2 = Thi_2;
    }

    public String getTK_num() {
        return TK_num;
    }

    public void setTK_num(String TK_num) {
        this.TK_num = TK_num;
    }

    public String getTK_String() {
        return TK_String;
    }

    public void setTK_String(String TK_String) {
        this.TK_String = TK_String;
    }

    public String getKy_hoc() {
        return ky_hoc;
    }

    public void setKy_hoc(String ky_hoc) {
        this.ky_hoc = ky_hoc;
    }

    public String getMsv() {
        return msv;
    }

    public void setMsv(String msv) {
        this.msv = msv;
    }

    public Subject(String code, String name, int soTC, String per_CC, String per_KT, String per_TH, String per_BT, String per_Thi, String CC, String KT, String TH, String BT, String Thi_1, String Thi_2, String TK_num, String TK_String, String ky_hoc, String msv) {
        this.code = code;
        this.name = name;
        this.soTC = soTC;
        this.per_CC = per_CC;
        this.per_KT = per_KT;
        this.per_TH = per_TH;
        this.per_BT = per_BT;
        this.per_Thi = per_Thi;
        this.CC = CC;
        this.KT = KT;
        this.TH = TH;
        this.BT = BT;
        this.Thi_1 = Thi_1;
        this.Thi_2 = Thi_2;
        this.TK_num = TK_num;
        this.TK_String = TK_String;
        this.ky_hoc = ky_hoc;
        this.msv = msv;
    }
    
    public Object[] toObject() {
        return new Object[]{
           this.getCode(),this.getName(),this.getSoTC(),this.getPer_CC(),this.getPer_KT(),this.getPer_TH(),
            this.getPer_BT(),this.getPer_Thi(),this.getCC(),this.getKT(),this.getTH(),this.getBT(),this.getThi_1(),
            this.getThi_2(),this.getTK_num(),this.getTK_String()
        };
    }
    public Object[] toObjectBlank() {
        return new Object[]{
           "","",null,"","","",
            "","","","","","","",
            "","",""
        };
    }
}
