/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author daova
 */
public class Student {
    private String code;
    private String name;
    private String pass;

    @Override
    public String toString() {
        return "Student{" + "code=" + code + ", name=" + name + ", pass=" + pass + '}';
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Student(String name, String code, String pass) {
        this.name = name;
        this.code = code;
        this.pass = pass;
    }

    public Student() {
    }

    
    
}
