/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

/**
 *
 * @author daova
 */
import Model.Result;
import Model.Subject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

public class WriteExel {

    private static HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        return style;
    }

    public void ghiFile(String namesv, ArrayList<Subject> listSj, ArrayList<Result> listRs, File file) throws FileNotFoundException, IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Bảng điểm");

        int rownum = 2;
        Cell cell;
        Row row;
        //
        HSSFCellStyle style = createStyleForTitle(workbook);
        row = sheet.createRow(0);
        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("Bảng điểm sinh viên " + namesv);
        cell.setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 15));

        row = sheet.createRow(rownum);

        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("Mã môn");
        cell.setCellStyle(style);
        // EmpName
        cell = row.createCell(1, CellType.STRING);
        cell.setCellValue("Tên Môn");
        cell.setCellStyle(style);
        // Salary
        cell = row.createCell(2, CellType.STRING);
        cell.setCellValue("Số tín chỉ");
        cell.setCellStyle(style);
        // Grade
        cell = row.createCell(3, CellType.STRING);
        cell.setCellValue("%CC");
        cell.setCellStyle(style);
        // Bonus
        cell = row.createCell(4, CellType.STRING);
        cell.setCellValue("%KT");
        cell.setCellStyle(style);

        cell = row.createCell(5, CellType.STRING);
        cell.setCellValue("%TH");
        cell.setCellStyle(style);
        // EmpName
        cell = row.createCell(6, CellType.STRING);
        cell.setCellValue("%BT");
        cell.setCellStyle(style);
        // Salary
        cell = row.createCell(7, CellType.STRING);
        cell.setCellValue("%Thi");
        cell.setCellStyle(style);
        // Grade
        cell = row.createCell(8, CellType.STRING);
        cell.setCellValue("Điểm CC");
        cell.setCellStyle(style);
        // Bonus
        cell = row.createCell(9, CellType.STRING);
        cell.setCellValue("Điểm KT");
        cell.setCellStyle(style);

        cell = row.createCell(10, CellType.STRING);
        cell.setCellValue("Điểm TH");
        cell.setCellStyle(style);

        cell = row.createCell(11, CellType.STRING);
        cell.setCellValue("Điểm BT");
        cell.setCellStyle(style);

        cell = row.createCell(12, CellType.STRING);
        cell.setCellValue("Thi L1");
        cell.setCellStyle(style);

        cell = row.createCell(13, CellType.STRING);
        cell.setCellValue("Thi L2");
        cell.setCellStyle(style);

        cell = row.createCell(14, CellType.STRING);
        cell.setCellValue("TK(10)");
        cell.setCellStyle(style);

        cell = row.createCell(15, CellType.STRING);
        cell.setCellValue("TK(CH)");
        cell.setCellStyle(style);

        for (Subject subject : listSj) {
            rownum++;
            row = sheet.createRow(rownum);

            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue(subject.getCode());
            cell.setCellStyle(style);
            cell = row.createCell(1, CellType.STRING);
            cell.setCellValue(subject.getName());
            cell.setCellStyle(style);
            cell = row.createCell(2, CellType.STRING);
            cell.setCellValue(subject.getSoTC());
            cell.setCellStyle(style);
            cell = row.createCell(3, CellType.STRING);
            cell.setCellValue(subject.getPer_CC());
            cell.setCellStyle(style);
            cell = row.createCell(4, CellType.STRING);
            cell.setCellValue(subject.getPer_KT());
            cell.setCellStyle(style);
            cell = row.createCell(5, CellType.STRING);
            cell.setCellValue(subject.getPer_TH());
            cell.setCellStyle(style);
            cell = row.createCell(6, CellType.STRING);
            cell.setCellValue(subject.getPer_BT());
            cell.setCellStyle(style);
            cell = row.createCell(7, CellType.STRING);
            cell.setCellValue(subject.getPer_Thi());
            cell.setCellStyle(style);
            cell = row.createCell(8, CellType.STRING);
            cell.setCellValue(subject.getCC());
            cell.setCellStyle(style);
            cell = row.createCell(9, CellType.STRING);
            cell.setCellValue(subject.getKT());
            cell.setCellStyle(style);
            cell = row.createCell(10, CellType.STRING);
            cell.setCellValue(subject.getTH());
            cell.setCellStyle(style);
            cell = row.createCell(11, CellType.STRING);
            cell.setCellValue(subject.getBT());
            cell.setCellStyle(style);
            cell = row.createCell(12, CellType.STRING);
            cell.setCellValue(subject.getThi_1());
            cell.setCellStyle(style);
            cell = row.createCell(13, CellType.STRING);
            cell.setCellValue(subject.getThi_2());
            cell.setCellStyle(style);
            cell = row.createCell(14, CellType.STRING);
            cell.setCellValue(subject.getTK_num());
            cell.setCellStyle(style);
            cell = row.createCell(15, CellType.STRING);
            cell.setCellValue(subject.getTK_String());
            cell.setCellStyle(style);
        }

        rownum += 3;
        row = sheet.createRow(rownum);
        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("Kết quả");
        cell.setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(rownum, rownum, 0, 6));
        
        
        rownum++;
        row = sheet.createRow(rownum);

        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("Kỳ học");
        cell.setCellStyle(style);
        // EmpName
        cell = row.createCell(1, CellType.STRING);
        cell.setCellValue("Điểm trung bình hệ 10");
        cell.setCellStyle(style);
        // Salary
        cell = row.createCell(2, CellType.STRING);
        cell.setCellValue("Điểm trung bình hệ 4");
        cell.setCellStyle(style);
        // Grade
        cell = row.createCell(3, CellType.STRING);
        cell.setCellValue("Điểm trung bình tích lũy");
        cell.setCellStyle(style);
        // Bonus
        cell = row.createCell(4, CellType.STRING);
        cell.setCellValue("Điểm trung bình tích lũy hệ 4");
        cell.setCellStyle(style);

        cell = row.createCell(5, CellType.STRING);
        cell.setCellValue("Số tín chỉ đạt");
        cell.setCellStyle(style);
        // EmpName
        cell = row.createCell(6, CellType.STRING);
        cell.setCellValue("Số tín chỉ tích lũy");
        cell.setCellStyle(style);
        for (Result result : listRs) {
            rownum++;
            row = sheet.createRow(rownum);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue(result.getKyhoc());
            cell.setCellStyle(style);
            // EmpName
            cell = row.createCell(1, CellType.STRING);
            cell.setCellValue(result.getHe10());
            cell.setCellStyle(style);
            // Salary
            cell = row.createCell(2, CellType.STRING);
            cell.setCellValue(result.getHe4());
            cell.setCellStyle(style);
            // Grade
            cell = row.createCell(3, CellType.STRING);
            cell.setCellValue(result.getTichluy10());
            cell.setCellStyle(style);
            // Bonus
            cell = row.createCell(4, CellType.STRING);
            cell.setCellValue(result.getTichluy4());
            cell.setCellStyle(style);

            cell = row.createCell(5, CellType.STRING);
            cell.setCellValue(result.getSotc());
            cell.setCellStyle(style);
            // EmpName
            cell = row.createCell(6, CellType.STRING);
            cell.setCellValue(result.getTongtctichluy());
            cell.setCellStyle(style);
        }

//        File file = new File("C:\\Users\\daova\\Desktop\\test\\employee9.xls");
        file.getParentFile().mkdirs();

        FileOutputStream outFile = new FileOutputStream(file);
        workbook.write(outFile);
        System.out.println("Created file: " + file.getAbsolutePath());
    }

//    public static void main(String[] args) throws FileNotFoundException, IOException {
//        WriteExel w = new WriteExel();
////        w.ghiFile("Thang", listSj, listRs);
//
//    }
}
