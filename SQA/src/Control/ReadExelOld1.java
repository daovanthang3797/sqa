/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DAO.StudentDAO;
import DAO.SubjectDAO;
import Model.Student;
import Model.Subject;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author daova
 */
public class ReadExelOld1 {

    public static String name;
    public static String code;
    public static String pass = "admin";
    public static String course = "D15";

    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream(
                "D:\\nam4\\SQA\\CSDL\\old\\BD Nhap mon tri tue nhan tao(web).xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        System.out.println(workbook.getNumberOfSheets());
        StudentDAO studentDao = new StudentDAO();
        SubjectDAO subjectDao = new SubjectDAO();
        Student student = new Student();
        Subject subject = new Subject();
        XSSFRow row;
        XSSFSheet sheet;
        System.out.println("start");

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            sheet = workbook.getSheetAt(i);
//            sheet = workbook.getSheetAt(1);
            row = sheet.getRow(2);//dong chua ten hp, va ma hp
            subject.setName(row.getCell(3).getStringCellValue());// col chua ten mon hoc
            subject.setCode("INT1341");// col chua ma mon hoc
            row = sheet.getRow(3);
            subject.setSoTC((int) row.getCell(3).getNumericCellValue());
            subject.setKy_hoc("20172");
            row = sheet.getRow(7);//row trong so
            if (row.getCell(7).getCellTypeEnum() == CellType.NUMERIC) {
                subject.setPer_CC(Double.toString(row.getCell(7).getNumericCellValue()));//per cc
            } else {
                subject.setPer_CC("");
            }
            if (row.getCell(8).getCellTypeEnum() == CellType.NUMERIC) {
                subject.setPer_KT(Double.toString(row.getCell(8).getNumericCellValue()));//per kt
            } else {
                subject.setPer_KT("");
            }
            if (row.getCell(9).getCellTypeEnum() == CellType.NUMERIC) {
                subject.setPer_TH(Double.toString(row.getCell(9).getNumericCellValue()));//per th
            } else {
                subject.setPer_TH("");
            }
            if (row.getCell(10).getCellTypeEnum() == CellType.NUMERIC) {
                subject.setPer_BT(Double.toString(row.getCell(10).getNumericCellValue()));//per bt
            } else {
                subject.setPer_BT("");
            }
            if (row.getCell(11).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell cell = row.getCell(11);
                cell.setCellType(Cell.CELL_TYPE_STRING);
                subject.setPer_Thi(cell.getStringCellValue());//per thi
            } else {
                subject.setPer_Thi("");
            }
            subject.setThi_2("");
            student.setPass("admin");
            for (int j = 8;; j++) {
                row = sheet.getRow(j);
                if (row.getCell(2).getCellTypeEnum() == CellType.BLANK) {
                    break;
                }
                subject.setMsv(row.getCell(2).getStringCellValue());
                student.setCode(row.getCell(2).getStringCellValue());
                student.setName(row.getCell(3).getStringCellValue() + " " + row.getCell(4).getStringCellValue());
                if (row.getCell(7).getCellTypeEnum() == CellType.NUMERIC) {
                    double res =row.getCell(7).getNumericCellValue();
                    subject.setCC( Double.toString((double)Math.round(res * 10) / 10));// thi                   
                } else {
                    subject.setCC("");
                }
                if (row.getCell(8).getCellTypeEnum() == CellType.NUMERIC) {
                    subject.setKT(Double.toString(row.getCell(8).getNumericCellValue()));// kt
                } else {
                    subject.setKT("");
                }
                if (row.getCell(9).getCellTypeEnum() == CellType.NUMERIC) {
                    subject.setTH(Double.toString(row.getCell(9).getNumericCellValue()));// th
                } else {
                    subject.setTH("");
                }
                if (row.getCell(10).getCellTypeEnum() == CellType.NUMERIC) {
                    subject.setBT(Double.toString(row.getCell(10).getNumericCellValue()));// bt
                } else {
                    subject.setBT("");
                }
                if (row.getCell(11).getCellTypeEnum() == CellType.NUMERIC) {
                    subject.setThi_1(Double.toString(row.getCell(11).getNumericCellValue()));//per thi
                } else {
                    subject.setThi_1("");
                }
                if (row.getCell(12).getCellTypeEnum() == CellType.FORMULA) {
                    XSSFCell cell2 = row.getCell(12);
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    String num = cell2.getStringCellValue();
                    String res = "";
                    if (num.length() > 3) {
                        for (int u = 0; u < 3; u++) {
                            res = res + num.charAt(u);
                        }
                        subject.setTK_num(res);//thi
                    } else {
                        subject.setTK_num(num);//thi
                    }
                } else {
                    subject.setTK_num("");
                }
                if (row.getCell(13).getCellTypeEnum() == CellType.FORMULA) {
                    XSSFCell cell3 = row.getCell(13);
                    cell3.setCellType(Cell.CELL_TYPE_STRING);
                    subject.setTK_String(cell3.getStringCellValue());//thi
                } else {
                    subject.setTK_String("");
                }
                System.out.println(subject);
                System.out.println(student);
                subjectDao.insert(subject);
                studentDao.insert(student);
                
            }

        }
    }
}
