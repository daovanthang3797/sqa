/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DAO.StudentDAO;
import DAO.SubjectDAO;
import Model.Student;
import Model.Subject;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author daova
 */
public class ReadDataNew2 {

    public static String name;
    public static String code;
    public static String pass = "admin";
    public static String course = "D15";

    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream(
                "D:\\nam4\\SQA\\CSDL\\new\\Quan ly du an phan mem ( Hết hạn PK ngày 28-01).xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        System.out.println(workbook.getNumberOfSheets());
        StudentDAO studentDao = new StudentDAO();
        SubjectDAO subjectDao = new SubjectDAO();
        Student student = new Student();
        Subject subject = new Subject();
        XSSFRow row;
        XSSFSheet sheet;
        System.out.println("start");

        for (int i = 1; i < workbook.getNumberOfSheets(); i++) {
            sheet = workbook.getSheetAt(i);
//            sheet = workbook.getSheetAt(1);
            row = sheet.getRow(3);//dong chua ten hp, va ma hp
            if (row.getCell(3).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell name = row.getCell(3);
                name.setCellType(Cell.CELL_TYPE_STRING);
                subject.setName(name.getStringCellValue());// col chua ten mon hoc
            }

            String code[] = row.getCell(15).getStringCellValue().split("-");
            subject.setCode(code[0]);
            //ma mon hoc
//            subject.setCode("INT1340");// col chua ma mon hoc
            row = sheet.getRow(4);
            if (row.getCell(3).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell tc = row.getCell(3);
                tc.setCellType(Cell.CELL_TYPE_NUMERIC);
                subject.setSoTC((int) tc.getNumericCellValue());// so tc
            }
//            subject.setSoTC((int) row.getCell(3).getNumericCellValue());
            subject.setKy_hoc("20181");
            row = sheet.getRow(8);//row trong so
            if (row.getCell(7).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell per_cc = row.getCell(7);
                per_cc.setCellType(Cell.CELL_TYPE_STRING);
                subject.setPer_CC(per_cc.getStringCellValue());// so tc
            } else {
                subject.setPer_CC("");
            }
            if (row.getCell(8).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell per_cc = row.getCell(8);
                per_cc.setCellType(Cell.CELL_TYPE_STRING);
                subject.setPer_KT(per_cc.getStringCellValue());// so tc
            } else {
                subject.setPer_KT("");
            }
            if (row.getCell(9).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell per_cc = row.getCell(9);
                per_cc.setCellType(Cell.CELL_TYPE_STRING);
                subject.setPer_TH(per_cc.getStringCellValue());// so tc
            } else {
                subject.setPer_TH("");
            }
            if (row.getCell(10).getCellTypeEnum() == CellType.FORMULA) {
                XSSFCell per_cc = row.getCell(10);
                per_cc.setCellType(Cell.CELL_TYPE_STRING);
                subject.setPer_BT(per_cc.getStringCellValue());// so tc
            } else {
                subject.setPer_BT("");
            }
            if (row.getCell(15).getCellTypeEnum() == CellType.NUMERIC) {
                subject.setPer_Thi(Double.toString(row.getCell(15).getNumericCellValue()));
            } else {
                if (row.getCell(15).getCellTypeEnum() == CellType.FORMULA) {
                    XSSFCell per_cc = row.getCell(15);
                    per_cc.setCellType(Cell.CELL_TYPE_STRING);
                    subject.setPer_Thi(per_cc.getStringCellValue());// so tc
                } else {
                    subject.setPer_Thi("");
                }
            }

            subject.setThi_2("");
            student.setPass("admin");
            for (int j = 9;; j++) {
                row = sheet.getRow(j);
                if (row.getCell(2).getCellTypeEnum() == CellType.BLANK) {
                    break;
                }
                subject.setMsv(row.getCell(2).getStringCellValue());
                student.setCode(row.getCell(2).getStringCellValue());
                student.setName(row.getCell(3).getStringCellValue() + " " + row.getCell(4).getStringCellValue());
                if (row.getCell(7).getCellTypeEnum() != CellType.BLANK) {
                    subject.setCC(Double.toString(row.getCell(7).getNumericCellValue()));// thi
                } else {
                    subject.setCC("");
                }
                if (row.getCell(8).getCellTypeEnum() != CellType.BLANK) {
                    if (row.getCell(8).getCellTypeEnum() == CellType.STRING) {
                        subject.setKT(row.getCell(8).getStringCellValue());
                    } else {
                        subject.setKT(Double.toString(row.getCell(8).getNumericCellValue()));// kt

                    }
                } else {
                    subject.setKT("");
                }
                if (row.getCell(9).getCellTypeEnum() != CellType.BLANK && row.getCell(9).getCellTypeEnum() != CellType.STRING) {

                    subject.setTH(Double.toString(row.getCell(9).getNumericCellValue()));// th
                } else {
                    subject.setTH("");
                }
                if (row.getCell(10).getCellTypeEnum() != CellType.BLANK && row.getCell(10).getCellTypeEnum() != CellType.STRING) {
                    subject.setBT(Double.toString(row.getCell(10).getNumericCellValue()));// bt
                } else {
                    subject.setBT("");
                }
                if (row.getCell(15).getCellTypeEnum() != CellType.BLANK) {
                    if (row.getCell(15).getCellTypeEnum() == CellType.FORMULA) {
                        XSSFCell cell = row.getCell(11);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        subject.setThi_1(cell.getStringCellValue());
                    } else if (row.getCell(15).getCellTypeEnum() == CellType.STRING) {
                        subject.setThi_1(row.getCell(15).getStringCellValue());//per thi

                    } else {
                        subject.setThi_1(Double.toString(row.getCell(15).getNumericCellValue()));//per thi

                    }
                } else {
                    subject.setThi_1("");
                }
                if (row.getCell(16).getCellTypeEnum() == CellType.FORMULA) {
                    XSSFCell cell2 = row.getCell(16);
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    String num = cell2.getStringCellValue();
                    String res = "";
                    if (num.length() > 3) {
                        for (int u = 0; u < 3; u++) {
                            res = res + num.charAt(u);
                        }
                        subject.setTK_num(res);//thi
                    } else {
                        subject.setTK_num(num);//thi
                    }
                } else {
                    subject.setTK_num("");
                }
                if (row.getCell(17).getCellTypeEnum() == CellType.FORMULA) {
                    XSSFCell cell3 = row.getCell(17);
                    cell3.setCellType(Cell.CELL_TYPE_STRING);
                    subject.setTK_String(cell3.getStringCellValue());//thi
                } else {
                    subject.setTK_String("");
                }
                studentDao.insert(student);
                subjectDao.insert(subject);
                System.out.println(student);
                System.out.println(subject);
            }

        }
    }
}
